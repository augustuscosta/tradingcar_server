Rails.application.routes.draw do
  
  resources :feeds
  resources :trades
  devise_for :users
  resources :users
  resources :stores

  namespace :api do
    
    scope :users do
      post '/sign_in_mobile',to:'/api/sessions#create', defaults: { format: 'json' }
      delete '/sign_out_mobile',to:'/api/sessions#destroy'
      post '/reset_password',to:'/api/sessions#reset_password'
    end

    post '/device_id',to:'/api/device_ids#create', defaults: { format: 'json' } 

    get '/current_user',to:'/api/users#current_user_session', defaults: { format: 'json' }

    post '/users/send_message', to: '/api/users#send_message'

    get '/makes',to:'/api/makes#index', defaults: { format: 'json' }

    get '/models',to:'/api/models#index', defaults: { format: 'json' }

    get '/versions',to:'/api/versions#index', defaults: { format: 'json' }

    post '/trades' => 'trades#create'
    delete '/trades' => 'trades#delete'
    get '/trades_matches' => 'trades#trades_matches'
    get '/trades/:id',to:'trades#show', defaults: { format: 'json' }
    get '/matchs' => 'trades#index'
    get '/want_buy' => 'trades#want_buy'
    get '/want_sell' => 'trades#want_sell'
    
    post '/vehicle_images' => 'vehicle_images#create'

  end

  root :to => 'users#index'

end
