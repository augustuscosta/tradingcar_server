class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :update_sanitized_params, if: :devise_controller?

  before_filter :check_user_type

  def update_sanitized_params
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:name, :email, :phone, :password, :password_confirmation, :user_type_id, :store_id)}

    devise_parameter_sanitizer.for(:account_update) {|u| u.permit(:name, :email, :phone, :password, :password_confirmation, :user_type_id, :store_id)}
  end

  def check_user_type

    if current_user == nil
      return
    end

    if current_user.user_type_id == 3
        render :file => "public/401.html", :status => :unauthorized
    end
  	
  end

end
