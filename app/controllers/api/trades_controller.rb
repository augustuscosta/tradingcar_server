class Api::TradesController < Api::BaseController
  before_filter :authenticate_user!

  respond_to :json

  def index
    @trades = Array.new

    if params['search']
      @trades = Trade.search(params['search'], current_user.id)
    else

      if params['size']
        @trades = Trade.where(store_id: current_user.store_id, active: true, user_id: current_user.id)
        .where("(id IN (SELECT DISTINCT(buyer_id) FROM matches)) OR (id IN (SELECT DISTINCT(selling_id) FROM matches))")
        .limit(params['size'].to_i + 30)
        .offset(params['size'].to_i)
        .order("updated_at ASC")
      else
        @trades = Trade.where(store_id: current_user.store_id, active: true, user_id: current_user.id)
        .where("(id IN (SELECT DISTINCT(buyer_id) FROM matches)) OR (id IN (SELECT DISTINCT(selling_id) FROM matches))")
        .limit(50)
        .order("updated_at ASC")
      end
    end

    render 'trades/index.json.jbuilder'    
    return @trades
  end

  def want_buy
    @trades = Array.new

    if params['size']
      @trades = Trade.where(store_id: current_user.store_id, active: true, user_id: current_user.id, business_type_id: 2)
      .limit(params['size'].to_i + 30)
      .offset(params['size'].to_i)
      .order("created_at DESC")
    else
      @trades = Trade.where(store_id: current_user.store_id, active: true, user_id: current_user.id, business_type_id: 2)
      .limit(50)
      .order("created_at DESC")
    end
    

    render 'trades/index.json.jbuilder'    
    return @trades
  end

  def want_sell
    @trades = Array.new

    if params['size']
      @trades = Trade.where(store_id: current_user.store_id, active: true, user_id: current_user.id, business_type_id: 1)
      .limit(params['size'].to_i + 50)
      .offset(params['size'].to_i)
      .order("created_at DESC")
    else
      @trades = Trade.where(store_id: current_user.store_id, active: true, user_id: current_user.id, business_type_id: 1)
      .limit(50)
      .order("created_at DESC")
    end
    
    render 'trades/index.json.jbuilder'    
    return @trades
  end


  def trades_matches
    @trades = Array.new
    if params['trade_id']
      matches = Trade.find(params['trade_id']).matches
      for match in matches do
        trade = nil
        if match.trade_id == match.buyer_id
          trade = Trade.find(match.selling_id)
        else
          trade = Trade.find(match.buyer_id)
        end
          if trade && trade.active
            @trades.push(trade)
          end
      end
    end
    render 'trades/index.json.jbuilder'    
    return @trades 
  end

  def show
    @trade = Trade.find(params['id'])
    render 'trades/show_api.json.jbuilder'
  end

  def delete
    @trade = Trade.find(params['id'])
    @trade.active = false
    @trade.save
    Match.where("id = ? OR buyer_id = ? OR selling_id = ?", @trade.id, @trade.id, @trade.id).destroy_all
    render 'trades/show_api.json.jbuilder'
  end

  def create
    @trade = Trade.new
    @trade.user = current_user
    @trade.store = current_user.store
    @trade.active = true
    @trade.business_type = BusinessType.find_by_id(params['business_type_id'])
    
    stock = params['stock']

    if stock
      @trade.stock = true
    else
      contact = Contact.new
      contact.name = params['contact']['name']
      contact.email = params['contact']['email']
      contact.phone = params['contact']['phone']
      contact.save
      @trade.contact = contact
    end

    vehicle = Vehicle.new

    if params['vehicle']['make']

      if params['vehicle']['make']['id']
        vehicle.make = Make.find_by_id(params['vehicle']['make']['id'])
      end

    end
    
    if params['vehicle']['model']

      if params['vehicle']['model']['id']
        vehicle.model = Model.find_by_id(params['vehicle']['model']['id'])
      end

    end

    if params['vehicle']['version']

      if params['vehicle']['version']['id']
        vehicle.version = Version.find_by_id(params['vehicle']['version']['id'])
      end

    end

    armored = params['vehicle']['armored']
    if armored
      vehicle.armored = true
    end

    if params['vehicle']['details']
      vehicle.details = params['vehicle']['details']
    end

    if params['vehicle']['km']
      vehicle.km = params['vehicle']['km']
    end

    if params['vehicle']['km_to']
      vehicle.km_to = params['vehicle']['km_to']
    end

    if params['vehicle']['year']
      vehicle.year = params['vehicle']['year']
    end

    if params['vehicle']['year_to']
      vehicle.year_to = params['vehicle']['year_to']
    end

    if params['vehicle']['fuel']
      vehicle.fuel = params['vehicle']['fuel']
    end

    if params['vehicle']['color']
      vehicle.color = params['vehicle']['color']
    end

    if params['vehicle']['gear_box']
      vehicle.gear_box = params['vehicle']['gear_box']
    end

    if params['vehicle']['car_body']
      vehicle.car_body = params['vehicle']['car_body']
    end

    if params['vehicle']['doors']
      vehicle.doors = params['vehicle']['doors']
    end

    if params['value']
      @trade.value = params['value'].to_f
    end

    if params['value_to']
      @trade.value_to = params['value_to'].to_f
    end

    vehicle.save

    @trade.vehicle = vehicle

    @trade.save

    render 'trades/show_api.json.jbuilder'    

    return @trade

  end

end