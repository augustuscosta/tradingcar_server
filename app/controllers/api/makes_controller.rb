class Api::MakesController < Api::BaseController
  before_filter :authenticate_user!

  respond_to :json
  
  def index
    search = params['search']
    @makes = Array.new

    if search.length == 0
    	@makes = get_hot_makes
    else
    	@makes = Make.search_make(search)
    end
    
    
    render 'makes/index_api.json.jbuilder'
  end

  def get_hot_makes
  	toReturn = Array.new
  	toReturn.push(Make.find(4))
  	toReturn.push(Make.find(5))
  	toReturn.push(Make.find(6))
  	toReturn.push(Make.find(7))
  	toReturn.push(Make.find(11))
  	toReturn.push(Make.find(13))
  	toReturn.push(Make.find(14))
  	toReturn.push(Make.find(16))
  	toReturn.push(Make.find(18))
  	toReturn
  end

end