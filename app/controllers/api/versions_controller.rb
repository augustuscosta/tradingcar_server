class Api::VersionsController < Api::BaseController
  before_filter :authenticate_user!

  respond_to :json
  
  def index
    search = params['search']
    make_id = params['make_id']
    model_id = params['model_id']
    @versions = Version.search_version(search, make_id, model_id)
    render 'versions/index_api.json.jbuilder'
  end

end