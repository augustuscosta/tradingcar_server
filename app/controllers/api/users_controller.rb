class Api::UsersController < Api::BaseController
  before_filter :authenticate_user!

  respond_to :json
 
  def current_user_session
    @user = current_user
    render 'users/show.json.jbuilder'
  end

  def send_message
  	message = params['message']
  	user_id = params['user_id']
  	user = User.find(user_id)
  	user.send_message(message)
  	render json: {send: true}
  end

end