class Api::SessionsController < Api::BaseController
  include Devise::Controllers
  
  before_filter :ensure_params_exist,only: [:create]
  skip_before_action :authenticate_user!

  respond_to :json

  def create
    resource = User.find_for_database_authentication(email: params[:email])
    return invalid_login_attempt unless resource

    if resource.valid_password?(params[:password])
      sign_in(resource)
      session[:user_id] = resource.id
      render json: {success: true,email: resource.email},status:200
      return
    end
    invalid_login_attempt
  end

  def destroy
    current_user = User.find_by(id: session[:user_id])
    if current_user
      sign_out(current_user)
      DeviceId.destroy_device_id_by_session(session)
    end
    render json: {logout: true}
  end

  def reset_password
    user_aux = User.new
    user_aux.email = params[:email]
    @user = User.send_reset_password_instructions(user_aux)
    if @user
      if @user.id
          head :status => 200
       else
         head :status => 404
       end
     else
       render :status => 422, :json => { :errors => @user.errors.full_messages }
     end
  end

  protected
  def ensure_params_exist
    return unless params[:email].blank?
    return unless params[:password].blank?
    render json: {success: false,message: "Missing  parameters"},status: 422
  end

  def invalid_login_attempt
    warden.custom_failure!
    render json: {success: false, message: "Error with your login or password"},status: 401
  end
  
  def user_params
      params.require(:user).permit(:id, :name, :email, :phone, :password, :password_confirmation, :user_type_id, :store_id)
  end


end
