class Api::VehicleImagesController < Api::BaseController
  before_filter :authenticate_user!

  respond_to :json

  # POST /resource
  def create
    image = VehicleImage.new(create_params)
    image.save
    render json: {success: true},status:200
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
   def create_params
      params.require(:vehicle_image).permit(:id, :vehicle_id, :image)
   end

end