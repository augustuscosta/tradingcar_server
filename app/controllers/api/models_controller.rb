class Api::ModelsController < Api::BaseController
  before_filter :authenticate_user!

  respond_to :json
  
  def index
    search = params['search']
    make_id = params['make_id']
    @models = Model.search_model(search, make_id)
    render 'models/index_api.json.jbuilder'
  end

end