class Api::DeviceIdsController < Api::BaseController
  before_filter :authenticate_user!

  respond_to :json
  
  def create
    @device_id = DeviceId.save_or_update_device_id(current_user, device_id_params, session)
    render 'device_ids/show.json.jbuilder'
  end


  private

   # Never trust parameters from the scary internet, only allow the white list through.
   def device_id_params
      params.require(:device_id).permit(:os, :token)
   end

end