class Trade < ActiveRecord::Base
  has_many :matches
  belongs_to :store
  belongs_to :user
  belongs_to :business_type
  belongs_to :vehicle
  belongs_to :contact

  after_create :check_matches


  private


  def check_matches
  	create_matches_new_thread
  end


  def create_matches_new_thread
  	#Thread.new do
      trades = search_matches
      create_matches(trades)
    #end
  end


  def self.search(search, user_id)

    joins("LEFT JOIN contacts ON contact_id = contacts.id
           LEFT JOIN vehicles ON vehicle_id = vehicles.id
           LEFT JOIN makes ON vehicles.make_id = makes.id
           LEFT JOIN models ON vehicles.model_id = models.id
           LEFT JOIN versions ON vehicles.version_id = versions.id")
    .where("lower(contacts.name) like ? OR
            lower(contacts.phone) like ? OR
            lower(contacts.email) like ? OR
            lower(makes.name) like ? OR
            lower(models.name) like ? OR
            lower(versions.name) like ? OR
            lower(vehicles.details) like ? OR
            lower(vehicles.year) like ? OR
            lower(vehicles.year_to) like ? OR
            lower(vehicles.fuel) like ? OR
            lower(vehicles.doors) like ? OR
            lower(vehicles.car_body) like ? OR
            lower(vehicles.gear_box) like ? OR
            lower(vehicles.color) like ?",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%",
     "%#{search.downcase}%")
    .where(user_id: user_id, active: true)
    .limit(50)
    .order("updated_at ASC")
    .select('distinct trades.*')

  end

  def search_matches

    query = Trade.where("(store_id = ? AND business_type_id = ? AND active = ?)", user.store_id, get_search_business_type_id, true)
    .joins(:vehicle)

    if vehicle.make_id != nil && vehicle.model_id != nil && vehicle.version_id != nil
      logger.info "BUSCA POR MARCAR, MODELO E VERSÃO COM OD IDS: " << vehicle.make_id.to_s << " " <<  vehicle.model_id.to_s << " " <<  vehicle.version_id.to_s
      query = query.where("make_id = ? OR make_id IS NULL AND model_id = ? OR model_id IS NULL  AND version_id = ? OR version_id IS NULL", vehicle.make_id, vehicle.model_id, vehicle.version_id)
    end

    if vehicle.make_id != nil && vehicle.model_id != nil && vehicle.version_id == nil
      logger.info "BUSCA POR MARCAR E MODELO COM OD IDS: " << vehicle.make_id.to_s << " " <<  vehicle.model_id.to_s
      query = query.where("make_id = ? OR make_id IS NULL AND model_id = ? OR model_id IS NULL", vehicle.make_id, vehicle.model_id)
    end

    if vehicle.make_id != nil && vehicle.model_id == nil && vehicle.version_id == nil
      query = query.where("make_id = ? OR make_id IS NULL", vehicle.make_id)
    end

    or_null_clausule = " OR km IS NULL"
    or_null_to_clausule = " OR km_to IS NULL"

    if business_type_id == 1 #vender
      if vehicle.km != nil
        query = query.where("km <= ?" << or_null_clausule, vehicle.km)
        query = query.where("km_to >= ?" << or_null_to_clausule, vehicle.km)
      end
    else
      if vehicle.km != nil
        query = query.where("km >= ?" << or_null_clausule, vehicle.km)
      end
      if vehicle.km_to != nil
        query = query.where("km <= ?" << or_null_clausule, vehicle.km_to)
      end
    end

    or_null_clausule = " OR year IS NULL"
    or_null_to_clausule = " OR year_to IS NULL"

    if business_type_id == 1 #vender
      if vehicle.year != nil
        query = query.where("year <= ?" << or_null_clausule, vehicle.year)
        query = query.where("year_to >= ?" << or_null_to_clausule, vehicle.year)
      end
    else
      if vehicle.year != nil
        query = query.where("year >= ?" << or_null_clausule, vehicle.year)
      end
      if vehicle.year_to != nil
        query = query.where("year <= ?" << or_null_clausule, vehicle.year_to)
      end
    end

    or_null_clausule = " OR value IS NULL"
    or_null_to_clausule = " OR value_to IS NULL"

    if business_type_id == 1 #vender
      if value != nil
        query = query.where("value <= ?" << or_null_clausule, value)
        query = query.where("value_to >= ?" << or_null_to_clausule, value)
      end
    else
      if value != nil
        query = query.where("value >= ?" << or_null_clausule, value)
      end
      if value_to != nil
        query = query.where("value <= ?" << or_null_clausule, value_to)
      end
    end

    or_null_clausule = ""

    if business_type_id == 1 #vender
      or_null_clausule = " OR fuel IS NULL"
    end

    if vehicle.fuel != nil
      query = query.where("fuel = ?" << or_null_clausule, vehicle.fuel)
    end

    or_null_clausule = ""

    if business_type_id == 1 #vender
      or_null_clausule = " OR doors IS NULL"
    end

    if vehicle.doors != nil
      query = query.where("doors = ?" << or_null_clausule, vehicle.doors)
    end

    or_null_clausule = ""

    if business_type_id == 1 #vender
      or_null_clausule = " OR color IS NULL"
    end

    if vehicle.color != nil
      query = query.where("color = ?" << or_null_clausule, vehicle.color)
    end

    or_null_clausule = ""

    if business_type_id == 1 #vender
      or_null_clausule = " OR gear_box IS NULL"
    end

    if vehicle.gear_box != nil
      query = query.where("gear_box = ?" << or_null_clausule, vehicle.gear_box)
    end

    or_null_clausule = ""
    
    if business_type_id == 1 #vender
      or_null_clausule = " OR car_body IS NULL"
    end

    if vehicle.car_body != nil
      query = query.where("car_body = ?" << or_null_clausule, vehicle.car_body)
    end

    if vehicle.armored != nil
      query = query.where("armored = ?", vehicle.armored)
    end


    query.limit(50).order("stock, updated_at ASC").select('distinct trades.*')

  end

  def create_matches(trades)
    logger.info "ACHOU TRADES: " << trades.to_s
  	for trade in trades do
      unless trade.stock == true && stock == true
        buyer_id = 0
      selling_id = 0

      if business_type_id == 2
        buyer_id = id
        selling_id = trade.id
      else
        buyer_id = trade.id
        selling_id = id
      end

      match = Match.find_or_initialize_by(:trade_id => id, :buyer_id => buyer_id, :selling_id => selling_id)
      match.save
      match = Match.find_or_initialize_by(:trade_id => trade.id, :buyer_id => buyer_id, :selling_id => selling_id)
      match.save
      trade.touch
      touch
      process_notification(trade)
      end
  	end
  end

  def get_search_business_type_id
  	if business_type_id == 1
  		2
  	else
  		1
  	end
  end

  def process_notification(trade)

  	if user.id != trade.user.id
		do_notification(trade.user, trade)
  	end
  	do_notification(user, self)


  	for user_to_check in trade.user.store.users do

  		unless user_to_check.id == trade.user.id || user_to_check.id == user.id
  			if user_to_check.user_type_id == 2
  				do_notification(user_to_check, trade)
  			end
  		end

  	end

  end

  def do_notification(user, trade)
  	user.its_trade(trade)
  end

end
