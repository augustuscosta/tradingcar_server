class Vehicle < ActiveRecord::Base
  has_many   :vehicle_images
  has_many   :trades
  belongs_to :make
  belongs_to :model
  belongs_to :version
  belongs_to :vehicle_fipe
end
