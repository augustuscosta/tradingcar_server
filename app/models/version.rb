class Version < ActiveRecord::Base
  belongs_to :model
  belongs_to :make

  def self.search_version(query, make_id, model_id)
	where("lower(name) like? OR lower(fipe_codigo) like? OR lower(key) like? OR lower(marca) like? OR lower(veiculo) like?", "%#{query.downcase}%","%#{query.downcase}%","%#{query.downcase}%","%#{query.downcase}%","%#{query.downcase}%").where(make_id: make_id).where(model_id: model_id).limit(10).order("name").select('distinct versions.*')
  end

end
