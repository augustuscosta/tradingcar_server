class Feed < ActiveRecord::Base
  belongs_to :trade
  belongs_to :user
  belongs_to :store
end
