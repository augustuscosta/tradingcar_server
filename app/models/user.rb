require 'fcm'
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :store
  belongs_to :user_type
  has_many :device_ids

  def self.search_user(query)
    joins("JOIN user_types ON user_type_id = user_types.id").where("user_types.name like ? OR phone like? OR email like? OR users.name like?", "%#{query}%","%#{query}%","%#{query}%","%#{query}%").limit(10).order("name DESC").select('distinct users.*')
  end

  def self.search_user_store(query, store_id)
    joins("JOIN user_types ON user_type_id = user_types.id").where("user_types.name like ? OR phone like? OR email like? OR users.name like?", "%#{query}%","%#{query}%","%#{query}%","%#{query}%").where(store_id: store_id).limit(10).order("name DESC").select('distinct users.*')
  end

    def self.search_between(type)
       where("cast(user_type_id as text) like ?", "%#{type}%").limit(50).order("name DESC")
    end
    
    def self.new_with_session(params, session)
      if session["devise.user_attributes"]
        new(session["devise.user_attributes"], without_protrection: true) do |user|
          user.attributes = params
          user.valid?
        end
      else
        super
      end
    end

   def update_with_password(params, *options)
        if password.blank?              
            update_attributes(params, *options)
        else
            super
        end
    end

    def self.process_uri(uri)
    require 'open-uri'
    require 'open_uri_redirections'
    open(uri, :allow_redirections => :safe) do |r|
      r.base_uri.to_s
    end
  end

    
    def active_for_authentication?
       true
    end


    def inactive_message
      "Não é possivel realizar o seu acesso. Verifique com a administração do Trading Car APP"
    end

    def send_message(message)
      UserMailer.message_mail(self, message).deliver_now
      send_notification_message(get_registration_ids, message)
    end

    def get_registration_ids
      registration_ids = Array.new
      for device_id in device_ids do
        registration_ids.push(device_id.token)
      end
      return registration_ids
    end

    def send_notification_message(registration_ids, message)
      if registration_ids.size < 1
        return
      end
      fcm = get_fcm
      options = {data: {notification_type: 'MESSAGE', message: message}, collapse_key: "message", priority: "high"}
      response = fcm.send(registration_ids, options)

      logger.info response
    end

    def its_trade(trade)
      send_trade_notification_message(get_registration_ids, trade)
    end

    def send_trade_notification_message(registration_ids, trade)
      if registration_ids.size < 1
        return
      end
      fcm = get_fcm
      options = {data: {notification_type: 'TRADE', message: "Deu TRADE!!!", trade_id: trade.id}, collapse_key: "trade", priority: "high"}
      response = fcm.send(registration_ids, options)

      logger.info response
    end

    def get_fcm
      fcm = FCM.new("AAAAOPKZFbk:APA91bEEd39fxUceRw1kJd2ocan405lS9yj-1HSzd9diYtpaZH8q_oJOPks0HG4SOWuQHCtV3a9eLDq5uTkSXFo4JBjC1mHCLcun8uP5Rfn3HKcTppzb1SAAdR_z0ylOd1TeK9IapzOJ")
      fcm
    end

end
