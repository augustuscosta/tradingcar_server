class Model < ActiveRecord::Base
  belongs_to :make

  def self.search_model(query, make_id)
	where("lower(name) like? OR lower(fipe_name) like? OR lower(key) like? OR lower(marca) like? OR lower(fipe_marca) like?", "%#{query.downcase}%","%#{query.downcase}%","%#{query.downcase}%","%#{query.downcase}%","%#{query.downcase}%").where(make_id: make_id).limit(10).order("name").select('distinct models.*')
  end

end
