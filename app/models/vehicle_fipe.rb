class VehicleFipe < ActiveRecord::Base
  belongs_to :version
  belongs_to :model
  belongs_to :make

  


  def self.get_fipe_data_carro
	
	carros = "carros"
	url = VehicleFipe.fipe_makes_carros_url
	VehicleFipe.request_make_data(carros, url)

  end

  def self.get_fipe_data_moto
	
	motos = "motos"
	url = VehicleFipe.fipe_makes_motos_url
	VehicleFipe.request_make_data(motos, url)

  end

  def self.get_fipe_data_caminhao
	
	caminhoes = "caminhoes"
	url = VehicleFipe.fipe_makes_caminhoes_url
	VehicleFipe.request_make_data(caminhoes, url)

  end

  


  private

  def self.request_make_data(tipo, url)
  	makes = VehicleFipe.parse_make_response(VehicleFipe.make_request(url))
	VehicleFipe.get_model_data(makes, tipo)
  end

  def self.get_model_data(makes, tipo)
  	makes.each do |make|
  		url = VehicleFipe.fipe_models_url(make.id_fipe, tipo)
  		models = VehicleFipe.request_model_data(make, url)
  		VehicleFipe.get_version_data(make, models, tipo)
  	end
  	
  end

  def self.get_version_data(make, models, tipo)
  	models.each do |model|
  		url = VehicleFipe.fipe_versions_url(make.id_fipe, model.id_fipe, tipo)
  		versions = VehicleFipe.request_version_data(make, model, url)
  		#VehicleFipe.get_vechile_data(make, model, versions, tipo)
  	end
  	
  end

  def self.get_vechile_data(make, model, versions, tipo)
  	versions.each do |version|
  		url = VehicleFipe.fipe_vehicle_url(make.id_fipe, model.id_fipe, version.id_fipe, tipo)
  		VehicleFipe.request_vehicle_data(make, model, version, url)
  	end
  	
  end

  def self.request_model_data(make, url)
  	VehicleFipe.parse_model_response(make, VehicleFipe.make_request(url))
  end

  def self.request_version_data(make, model, url)
  	VehicleFipe.parse_version_response(make, model, VehicleFipe.make_request(url))
  end

  def self.request_vehicle_data(make, model, version, url)
  	VehicleFipe.parse_vehicle_response(make, model, version, VehicleFipe.make_request(url))
  end

  

  def self.make_request(url)
  	logger.info url

  	sholdTryAgain = true

  	while sholdTryAgain do

  		sholdTryAgain = false

  		RestClient.get(URI.encode(url)) { |response, req, result, &block|

			case response.code

				when 200
					logger.info response
					hash_response = JSON.parse response

					unless hash_response.nil?

						return hash_response

					else

						return Array.new

					end

				else
					sholdTryAgain = true
				end

		}

  	end

  end

  def self.parse_make_response(hash_response)
	makes = Array.new
	hash_response.each do |v|

		hash = Hash.new
		hash['id_fipe'] = v['id']
		hash['name'] = v['name']
		hash['fipe_name'] = v['fipe_name']
		hash['order'] = v['order']
		hash['key'] = v['key']
			
		makes.push(hash)

	end
	VehicleFipe.save_makes_hash(makes)
  end

  def self.parse_model_response(make, hash_response)
	models = Array.new
	hash_response.each do |v|

		hash = Hash.new
		hash['id_fipe'] = v['id']
		hash['name'] = v['name']
		hash['fipe_name'] = v['fipe_name']
		hash['marca'] = v['marca']
		hash['key'] = v['key']
		hash['fipe_marca'] = v['fipe_marca']
		hash['make_id'] = make.id
			
		models.push(hash)

	end
	VehicleFipe.save_models_hash(models)
  end

  def self.parse_version_response(make, model, hash_response)
	versions = Array.new
	hash_response.each do |v|

		hash = Hash.new
		hash['id_fipe'] = v['id']
		hash['fipe_marca'] = v['fipe_marca']
		hash['fipe_codigo'] = v['fipe_codigo']
		hash['name'] = v['name']
		hash['marca'] = v['marca']
		hash['key'] = v['key']
		hash['veiculo'] = v['veiculo']
		hash['make_id'] = make.id
		hash['model_id'] = model.id
			
		versions.push(hash)

	end
	VehicleFipe.save_versions_hash(versions)
  end

  def self.parse_vehicle_response(make, model, version, hash_response)
	hash = Hash.new
	hash['id_fipe'] = hash_response['id']
	hash['veiculo'] = hash_response['veiculo']
	hash['time'] = hash_response['time']
	hash['key'] = hash_response['key']
	hash['preco'] = hash_response['preco']
	hash['ano_modelo'] = hash_response['ano_modelo']
	hash['marca'] = hash_response['marca']
	hash['combustivel'] = hash_response['combustivel']
	hash['name'] = hash_response['name']
	hash['fipe_codigo'] = hash_response['fipe_codigo']
	hash['referencia'] = hash_response['referencia']
	hash['make_id'] = make.id
	hash['model_id'] = model.id
	hash['version_id'] = version.id

	VehicleFipe.save_vehicle_hash(hash)
  end

  def self.save_makes_hash(makes_hash)
  	makes = Array.new
  	Make.transaction do
		makes_hash.each do |m|
			make = Make.find_or_initialize_by(:id_fipe => m['id_fipe'])
			make.update(m)
			makes.push(make)
		end
	end
	makes
  end

  def self.save_models_hash(models_hash)
  	models = Array.new
  	Model.transaction do
		models_hash.each do |m|
			model = Model.find_or_initialize_by(:id_fipe => m['id_fipe'], :make_id => m['make_id'])
			model.update(m)
			models.push(model)
		end
	end
	models
  end

  def self.save_versions_hash(versions_hash)
  	versions = Array.new
  	Version.transaction do
		versions_hash.each do |m|
			version = Version.find_or_initialize_by(:id_fipe => m['id_fipe'], :make_id => m['make_id'], :model_id => m['model_id'])
			version.update(m)
			versions.push(version)
		end
	end
	versions
  end

  def self.save_vehicle_hash(vehicle_hash)
  	VehicleFipe.transaction do
		version = VehicleFipe.find_or_initialize_by(:id_fipe => vehicle_hash['id_fipe'], :make_id => vehicle_hash['make_id'], :model_id => vehicle_hash['model_id'], :version_id => vehicle_hash['version_id'])
		version.update(vehicle_hash)
	end
  end

  def self.fipe_makes_carros_url
	"http://fipeapi.appspot.com/api/1/carros/marcas.json"
  end

  def self.fipe_makes_motos_url
	"http://fipeapi.appspot.com/api/1/motos/marcas.json"
  end

  def self.fipe_makes_caminhoes_url
	"http://fipeapi.appspot.com/api/1/caminhoes/marcas.json"
  end

  def self.fipe_makes_caminhoes_url
	"http://fipeapi.appspot.com/api/1/caminhoes/marcas.json"
  end

  def self.fipe_models_url(make_fipe_id, tipo)
	"http://fipeapi.appspot.com/api/1/" << tipo << "/veiculos/" << make_fipe_id.to_s << ".json"
  end

  def self.fipe_versions_url(make_fipe_id, model_fipe_id, tipo)
	"http://fipeapi.appspot.com/api/1/" << tipo << "/veiculo/" << make_fipe_id.to_s << "/" << model_fipe_id.to_s << ".json"
  end

  def self.fipe_vehicle_url(make_fipe_id, model_fipe_id, version_fipe_id, tipo)
	"http://fipeapi.appspot.com/api/1/" << tipo << "/veiculo/" << make_fipe_id.to_s << "/" << model_fipe_id.to_s << "/" << version_fipe_id.to_s << ".json"
  end

end
