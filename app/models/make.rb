class Make < ActiveRecord::Base

  def self.search_make(query)
    where("lower(name) like? OR lower(fipe_name) like? OR lower(key) like?", "%#{query.downcase}%","%#{query.downcase}%","%#{query.downcase}%").limit(10).order("name DESC").select('distinct makes.*')
  end

end
