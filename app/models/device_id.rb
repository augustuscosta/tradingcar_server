class DeviceId < ActiveRecord::Base
  belongs_to :user

  def self.save_or_update_device_id(user, device_id, session)
   		device_id = DeviceId.find_or_initialize_by(token: device_id["token"])
   		device_id.user = user
   		device_id.os = device_id["os"]
   		device_id.session_id = session.id
   		device_id.save
   		return device_id
   end

   def self.destroy_device_id_by_session(session)
   		device_id = DeviceId.find_by(session_id: session.id)
   		if device_id != nil
   			device_id.destroy
   		end
   end
end
