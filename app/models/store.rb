class Store < ActiveRecord::Base
  has_many :users
  belongs_to :plan

  def self.search_store(query)
    joins("JOIN plans ON plan_id = plans.id").where("plans.name like? OR phone like? OR email like? OR stores.name like? OR contact_name like? OR cnpj like? OR address like? OR message like? OR blocked_message like?", "%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%").limit(10).order("name DESC").select('distinct stores.*')
  end

end
