class VehicleImage < ActiveRecord::Base
  belongs_to :vehicle
  has_attached_file :image, default_url: "/assets/car.png", styles: { medium: "300x300>", thumb: "100x100>"}
  validates_attachment :image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
end
