$( document ).on('turbolinks:load', function() {
  userReady();
});

var userReady = function() {
  $( "#notification_button" ).click(function() {
  	message = $( "#notification_message" ).val();
	if(message.length < 1){
		return;
	}
	user_id = $( "#user_id" ).val();
  	sendMessage(message, user_id);
  });
};

var sendMessage = function(message, user_id){
	data = {message: message, user_id:user_id}
	$.ajax({
              type: "post",
              url: "/api/users/send_message",
              data: data,
              success: function (){
              	alert( "Mensagem enviada." );
				$( "#notification_message" ).val("");
              }
          });
};


