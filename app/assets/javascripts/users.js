$( document ).on('turbolinks:load', function() {
  usersReady();
});

var usersReady = function() {
  searchUsersListener();
};

var searchUsersListener = function() {
  $('#search').keyup(function() {
    $.get($("#users_search").attr("action"), $("#users_search").serialize(), null, 'script');
    return false;
  });
}