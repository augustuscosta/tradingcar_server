$( document ).on('turbolinks:load', function() {
  storesReady();
});

var storesReady = function() {
  searchStoreListener();
};

var searchStoreListener = function() {
  $('#search').keyup(function() {
    $.get($("#stores_search").attr("action"), $("#stores_search").serialize(), null, 'script');
    return false;
  });
}