json.extract! feed, :id, :action, :trade_id, :user_id, :store_id, :created_at, :updated_at
json.url feed_url(feed, format: :json)
