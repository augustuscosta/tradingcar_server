json.extract! trade, :id, :store_id, :user_id, :active, :business_type_id, :vehicle_id, :contact_id, :stock, :value, :value_to, :created_at, :updated_at

if trade.user_id !=nil
	json.user do |json|
        json.id trade.user.id
        json.name trade.user.name
        json.email trade.user.email
        json.phone trade.user.phone
        json.user_type_id trade.user.user_type_id
    end
end

if trade.contact_id !=nil
	json.contact do |json|
        json.id trade.contact.id
        json.name trade.contact.name
        json.email trade.contact.email
        json.phone trade.contact.phone
    end
end

if trade.matches != nil
	json.matches trade.matches do |match| 
        json.id match.id
        json.buyer_id match.buyer_id
      	json.buyer_id match.selling_id
    end
end

if trade.vehicle_id !=nil
	json.vehicle do |json|
        json.id trade.vehicle.id
        if trade.vehicle.armored
        	json.armored trade.vehicle.armored
        end

        unless trade.vehicle.details.to_s.empty?
        	json.details trade.vehicle.details
        end

        unless trade.vehicle.km.to_s.empty?
        	json.km trade.vehicle.km
        end

        unless trade.vehicle.km_to.to_s.empty?
            json.km_to trade.vehicle.km_to
        end
    	
    	unless trade.vehicle.year.to_s.empty?
        	json.year trade.vehicle.year
        end

        unless trade.vehicle.year_to.to_s.empty?
            json.year_to trade.vehicle.year_to
        end

        unless trade.vehicle.fuel.to_s.empty?
        	json.fuel trade.vehicle.fuel
        end

        unless trade.vehicle.doors.to_s.empty?
        	json.doors trade.vehicle.doors
        end
        
        unless trade.vehicle.car_body.to_s.empty?
        	json.car_body trade.vehicle.car_body
        end

        unless trade.vehicle.gear_box.to_s.empty?
        	json.gear_box trade.vehicle.gear_box
        end

        unless trade.vehicle.color.to_s.empty?
        	json.color trade.vehicle.color
        end

        if trade.vehicle.make_id != nil
        	json.make do |json|
        		json.id trade.vehicle.make.id
        		json.name trade.vehicle.make.name
        	end
        end

        if trade.vehicle.model_id != nil
        	json.model do |json|
        		json.id trade.vehicle.model.id
        		json.name trade.vehicle.model.name
        	end
        end

        if trade.vehicle.version_id != nil
        	json.version do |json|
        		json.id trade.vehicle.version.id
        		json.name trade.vehicle.version.name
        	end
        end

        if trade.vehicle.vehicle_images != nil
        	json.vehicle_images trade.vehicle.vehicle_images do |image| 
        		json.id image.id
        		json.vehicle_id image.vehicle_id
        		json.image image.image.url
        	end
        end
        
    end
end

json.url trade_url(trade, format: :json)
