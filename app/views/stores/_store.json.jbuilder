json.extract! store, :id, :name, :contact_name, :email, :phone, :cnpj, :address, :pay_day, :message, :blocked, :blocked_message, :plan_id, :created_at, :updated_at
json.url store_url(store, format: :json)
