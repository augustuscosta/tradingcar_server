json.extract! user, :id, :name, :email, :phone, :password, :store_id, :user_type_id, :created_at, :updated_at
json.url user_url(user, format: :json)
