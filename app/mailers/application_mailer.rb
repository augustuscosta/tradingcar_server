class ApplicationMailer < ActionMailer::Base
  default from: "tradingcarapp@gmail.com"
  layout 'mailer'
end
