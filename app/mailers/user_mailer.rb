class UserMailer < ApplicationMailer
	def message_mail(user, message)
    	@user = user
    	@message = message
    	mail(to: @user.email, subject: 'Trading Car APP')
    end
 end