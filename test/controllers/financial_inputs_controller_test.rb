require 'test_helper'

class FinancialInputsControllerTest < ActionController::TestCase
  setup do
    @financial_input = financial_inputs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:financial_inputs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create financial_input" do
    assert_difference('FinancialInput.count') do
      post :create, financial_input: { date: @financial_input.date, name: @financial_input.name, paid: @financial_input.paid, store_id: @financial_input.store_id }
    end

    assert_redirected_to financial_input_path(assigns(:financial_input))
  end

  test "should show financial_input" do
    get :show, id: @financial_input
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @financial_input
    assert_response :success
  end

  test "should update financial_input" do
    patch :update, id: @financial_input, financial_input: { date: @financial_input.date, name: @financial_input.name, paid: @financial_input.paid, store_id: @financial_input.store_id }
    assert_redirected_to financial_input_path(assigns(:financial_input))
  end

  test "should destroy financial_input" do
    assert_difference('FinancialInput.count', -1) do
      delete :destroy, id: @financial_input
    end

    assert_redirected_to financial_inputs_path
  end
end
