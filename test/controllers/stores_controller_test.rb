require 'test_helper'

class StoresControllerTest < ActionController::TestCase
  setup do
    @store = stores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:stores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create store" do
    assert_difference('Store.count') do
      post :create, store: { address: @store.address, alert: @store.alert, alert_message: @store.alert_message, blocked: @store.blocked, blocked_message: @store.blocked_message, cnpj: @store.cnpj, contact_name: @store.contact_name, email: @store.email, name: @store.name, pay_day: @store.pay_day, phone: @store.phone, plan_id: @store.plan_id }
    end

    assert_redirected_to store_path(assigns(:store))
  end

  test "should show store" do
    get :show, id: @store
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @store
    assert_response :success
  end

  test "should update store" do
    patch :update, id: @store, store: { address: @store.address, alert: @store.alert, alert_message: @store.alert_message, blocked: @store.blocked, blocked_message: @store.blocked_message, cnpj: @store.cnpj, contact_name: @store.contact_name, email: @store.email, name: @store.name, pay_day: @store.pay_day, phone: @store.phone, plan_id: @store.plan_id }
    assert_redirected_to store_path(assigns(:store))
  end

  test "should destroy store" do
    assert_difference('Store.count', -1) do
      delete :destroy, id: @store
    end

    assert_redirected_to stores_path
  end
end
