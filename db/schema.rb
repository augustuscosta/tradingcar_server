# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170505143307) do

  create_table "business_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "device_ids", force: :cascade do |t|
    t.string   "token"
    t.string   "os"
    t.string   "session_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "device_ids", ["user_id"], name: "index_device_ids_on_user_id"

  create_table "feeds", force: :cascade do |t|
    t.string   "action"
    t.integer  "trade_id"
    t.integer  "user_id"
    t.integer  "store_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "feeds", ["store_id"], name: "index_feeds_on_store_id"
  add_index "feeds", ["trade_id"], name: "index_feeds_on_trade_id"
  add_index "feeds", ["user_id"], name: "index_feeds_on_user_id"

  create_table "financial_inputs", force: :cascade do |t|
    t.string   "name"
    t.date     "date"
    t.date     "paid"
    t.integer  "store_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "financial_inputs", ["store_id"], name: "index_financial_inputs_on_store_id"

  create_table "makes", force: :cascade do |t|
    t.integer  "id_fipe"
    t.string   "name"
    t.string   "fipe_name"
    t.integer  "order"
    t.string   "key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "matches", force: :cascade do |t|
    t.integer  "trade_id"
    t.integer  "buyer_id"
    t.integer  "selling_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "matches", ["trade_id"], name: "index_matches_on_trade_id"

  create_table "models", force: :cascade do |t|
    t.integer  "id_fipe"
    t.string   "name"
    t.string   "fipe_name"
    t.string   "marca"
    t.string   "key"
    t.string   "fipe_marca"
    t.integer  "make_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "models", ["make_id"], name: "index_models_on_make_id"

  create_table "plans", force: :cascade do |t|
    t.string   "name"
    t.float    "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stores", force: :cascade do |t|
    t.string   "name"
    t.string   "contact_name"
    t.string   "email"
    t.string   "phone"
    t.string   "cnpj"
    t.string   "address"
    t.integer  "pay_day"
    t.string   "message"
    t.boolean  "blocked"
    t.string   "blocked_message"
    t.integer  "plan_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "stores", ["plan_id"], name: "index_stores_on_plan_id"

  create_table "trades", force: :cascade do |t|
    t.integer  "store_id"
    t.integer  "user_id"
    t.boolean  "active"
    t.integer  "business_type_id"
    t.integer  "vehicle_id"
    t.integer  "contact_id"
    t.boolean  "stock"
    t.float    "value"
    t.float    "value_to"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "trades", ["business_type_id"], name: "index_trades_on_business_type_id"
  add_index "trades", ["contact_id"], name: "index_trades_on_contact_id"
  add_index "trades", ["store_id"], name: "index_trades_on_store_id"
  add_index "trades", ["user_id"], name: "index_trades_on_user_id"
  add_index "trades", ["vehicle_id"], name: "index_trades_on_vehicle_id"

  create_table "user_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "password"
    t.integer  "store_id"
    t.integer  "user_type_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["store_id"], name: "index_users_on_store_id"
  add_index "users", ["user_type_id"], name: "index_users_on_user_type_id"

  create_table "vehicle_fipes", force: :cascade do |t|
    t.string   "id_fipe"
    t.string   "veiculo"
    t.integer  "time"
    t.string   "key"
    t.string   "preco"
    t.string   "ano_modelo"
    t.string   "marca"
    t.string   "combustivel"
    t.string   "name"
    t.string   "fipe_codigo"
    t.string   "referencia"
    t.integer  "version_id"
    t.integer  "model_id"
    t.integer  "make_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "vehicle_fipes", ["make_id"], name: "index_vehicle_fipes_on_make_id"
  add_index "vehicle_fipes", ["model_id"], name: "index_vehicle_fipes_on_model_id"
  add_index "vehicle_fipes", ["version_id"], name: "index_vehicle_fipes_on_version_id"

  create_table "vehicle_images", force: :cascade do |t|
    t.integer  "vehicle_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "vehicle_images", ["vehicle_id"], name: "index_vehicle_images_on_vehicle_id"

  create_table "vehicles", force: :cascade do |t|
    t.integer  "make_id"
    t.integer  "model_id"
    t.integer  "version_id"
    t.integer  "vehicle_fipe_id"
    t.boolean  "armored"
    t.string   "details"
    t.integer  "km"
    t.integer  "km_to"
    t.integer  "year"
    t.integer  "year_to"
    t.string   "fuel"
    t.string   "doors"
    t.string   "car_body"
    t.string   "gear_box"
    t.string   "color"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "vehicles", ["make_id"], name: "index_vehicles_on_make_id"
  add_index "vehicles", ["model_id"], name: "index_vehicles_on_model_id"
  add_index "vehicles", ["vehicle_fipe_id"], name: "index_vehicles_on_vehicle_fipe_id"
  add_index "vehicles", ["version_id"], name: "index_vehicles_on_version_id"

  create_table "versions", force: :cascade do |t|
    t.string   "id_fipe"
    t.string   "fipe_marca"
    t.string   "fipe_codigo"
    t.string   "name"
    t.string   "marca"
    t.string   "key"
    t.string   "veiculo"
    t.integer  "model_id"
    t.integer  "make_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "versions", ["make_id"], name: "index_versions_on_make_id"
  add_index "versions", ["model_id"], name: "index_versions_on_model_id"

end
