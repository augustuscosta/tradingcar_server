class CreateVehicleFipes < ActiveRecord::Migration
  def change
    create_table :vehicle_fipes do |t|
      t.string :id_fipe
      t.string :veiculo
      t.integer :time
      t.string :key
      t.string :preco
      t.string :ano_modelo
      t.string :marca
      t.string :combustivel
      t.string :name
      t.string :fipe_codigo
      t.string :referencia
      t.references :version, index: true, foreign_key: true
      t.references :model, index: true, foreign_key: true
      t.references :make, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
