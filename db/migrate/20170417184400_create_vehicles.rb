class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.references :make, index: true, foreign_key: true
      t.references :model, index: true, foreign_key: true
      t.references :version, index: true, foreign_key: true
      t.references :vehicle_fipe, index: true, foreign_key: true
      t.boolean :armored
      t.string :details
      t.integer :km
      t.integer :km_to
      t.integer :year
      t.integer :year_to
      t.string :fuel
      t.string :doors
      t.string :car_body
      t.string :gear_box
      t.string :color

      t.timestamps null: false
    end
  end
end
