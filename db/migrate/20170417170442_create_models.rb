class CreateModels < ActiveRecord::Migration
  def change
    create_table :models do |t|
      t.integer :id_fipe
      t.string :name
      t.string :fipe_name
      t.string :marca
      t.string :key
      t.string :fipe_marca
      t.references :make, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
