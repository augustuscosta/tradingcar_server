class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :name
      t.string :contact_name
      t.string :email
      t.string :phone
      t.string :cnpj
      t.string :address
      t.integer :pay_day
      t.string :message
      t.boolean :blocked
      t.string :blocked_message
      t.references :plan, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
