class CreateMakes < ActiveRecord::Migration
  def change
    create_table :makes do |t|
      t.integer :id_fipe
      t.string :name
      t.string :fipe_name
      t.integer :order
      t.string :key

      t.timestamps null: false
    end
  end
end
