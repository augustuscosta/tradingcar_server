class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.references :trade, index: true, foreign_key: true
      t.integer :buyer_id
      t.integer :selling_id
      t.timestamps null: false
    end
  end
end
