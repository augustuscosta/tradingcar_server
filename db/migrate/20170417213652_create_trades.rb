class CreateTrades < ActiveRecord::Migration
  def change
    create_table :trades do |t|
      t.references :store, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.boolean :active
      t.references :business_type, index: true, foreign_key: true
      t.references :vehicle, index: true, foreign_key: true
      t.references :contact, index: true, foreign_key: true
      t.boolean :stock
      t.float :value
      t.float :value_to

      t.timestamps null: false
    end
  end
end
