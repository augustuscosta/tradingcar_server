class CreateFinancialInputs < ActiveRecord::Migration
  def change
    create_table :financial_inputs do |t|
      t.string :name
      t.date :date
      t.date :paid
      t.references :store, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
