class CreateVersions < ActiveRecord::Migration
  def change
    create_table :versions do |t|
      t.string :id_fipe
      t.string :fipe_marca
      t.string :fipe_codigo
      t.string :name
      t.string :marca
      t.string :key
      t.string :veiculo
      t.references :model, index: true, foreign_key: true
      t.references :make, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
